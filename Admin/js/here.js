// LIBRERIA QUE CONTIENE TODO CON LA API DE HERE MAPS

var platform = new H.service.Platform({
    'apikey': 'uribNWroYxfP24gqVES5dLwlpOs_A8s1xomVMePcsaE'//LLAVE PROPORCIONADO POR HERE MAPS
});

// Obtain the default map types from the platform


  var defaultLayers = platform.createDefaultLayers();

  // Instantiate (and display) a map object:
  var map = new H.Map(
  document.getElementById('map'),
  defaultLayers.vector.normal.map,
  {
      zoom: 12,
      center: { lat: 21.1119561, lng: -101.6932362 },//COORDENADAS DEL ESTADO DE LEÓN
      pixelRatio: window.devicePixelRatio || 1
  });
  
  
  // add a resize listener to make sure that the map occupies the whole container
  window.addEventListener('resize', () => map.getViewPort().resize());

  //ESTE ES EL METODO DONDE SE LE DA CLIC CON EL MOUSE Y SE OBTIENE LAS COORDENADAS PARA LUEGO METERLAS A LOS INPUT
  map.addEventListener("tap", evt => {
    map.removeObjects(map.getObjects ());//SE ELIMINAN LOS MARCADORES EXISTENTES
    var coord = map.screenToGeo(evt.currentPointer.viewportX,
      evt.currentPointer.viewportY);
      $("#latitud").val(coord.lat)//LE AGREGA EL VALOR AL INPUT
      $("#longitud").val(coord.lng)
      const params = coord.lat+","+coord.lng;//CREA UNA CONSTANTE DE LAS COORDENADAS PARA PASARLA COMO PARAMETRO
      reverseGeocoder(params);//HACER UN GEODECODER PARA OBTENER EL NOMBRE DE LAS COORDENADAS

    var now = new H.map.Marker({lat:coord.lat, lng:coord.lng});//SE CREA UN NUEVO MARCADOR PARA AGREGAR AL MAPA
    map.addObject(now);//SE AGREGA EL MARCADOR
  })
  
  
  //Step 3: make the map interactive
  // MapEvents enables the event system
  // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
  var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
  
  // Step 4: Create the default UI:
  var ui = H.ui.UI.createDefault(map, defaultLayers, 'en-US');
  

                    
// FUNCION QUE TOMA EL VALOR DEL INPUT DE DIRECCION PARA GENERAR UN MARCADOR EN CASO DE QUE QUIERAN METERLO COMO ESCRITO LA DIRECCION        
function geocoder() 
{
  // Get an instance of the geocoding service:
  var service = platform.getSearchService();
  console.log(service);
  // Call the geocode method with the geocoding parameters,
  // the callback and an error callback function (called if a
  // communication error occurs):
  service.geocode({
    q: $("#dire").val()//INPUT 
  }, (result) => {
    // Add a marker for each location found
    map.removeObjects(map.getObjects ());
    result.items.forEach((item) => {
      map.addObject(new H.map.Marker(item.position));//SE AGREGA EL MARCADOR DE LA DIRECCION QUE SE QUE ESCRIBIO 
      $("#latitud").val(item.position.lat)//SE AGREGA EL VALOR DE LATITUD Y LONGITUD A LOS INPUT
      $("#longitud").val(item.position.lng)
    });
  }, alert);  
}

//FUNCION QUE RECIBE LAS COORDENADAS PARA GENERAR LA DIRECCION ESCRITA DE DONDE SE ASIGNO EL MARCADOR
function reverseGeocoder(params)
{
  // Get an instance of the search service:
  var service = platform.getSearchService();

  // Call the reverse geocode method with the geocoding parameters,
  // the callback and an error callback function (called if a
  // communication error occurs):
  service.reverseGeocode({
    at: params
  }, (result) => {
    result.items.forEach((item) => {
      // Assumption: ui is instantiated
      // Create an InfoBubble at the returned location with
      // the address as its contents:
      ui.addBubble(new H.ui.InfoBubble(item.position, {
        content: item.address.label        
      }));

      $("#dire").val(item.address.label);//SE AGREGA EL VALOR AL INPUT DE DIRECCION
    });
  }, alert);
}