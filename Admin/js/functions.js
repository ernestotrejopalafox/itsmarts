// FUNCION QUE TOMA LOS VALORES DEL FORM Y LOS MANDA AL SERVICIO PARA AGREGAR A UN NUEVO CLIENTE
function addCliente ()
{
    var url = "https://omartrejo.000webhostapp.com/Services/Server/functions/addCliente.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",//METODO DE ENVIO
        url: url,
        data: $("#frmCliente").serialize(), // Adjuntar los campos del formulario enviado.
        success: function(data) {
            //VALIDACION DE QUE LOS DATOS HAYAN SIDO CORRECTAMENTE ENVIADOS
            if (data === "Ok") {
                alert("Registro exitoso");
                setTimeout(function() {
                    window.location = "index.php?view=cliente"; //ESTE METODO REDIRECCIONA A LA PANTALLA DE CLIENTES
                }, 500);
            }
            else
            {
                alert("Error, verifica que todos los datos hayan sido llenados");
            }            
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}
// FUNCION QUE TOMA LOS VALORES DEL FORM Y LOS MANDA AL SERVICIO PARA ACTUALIZAR LOS DATOS CLIENTE
function updateCliente ()
{
    var url = "https://omartrejo.000webhostapp.com/Services/Server/functions/updateCliente.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#frmClienteEdit").serialize(), // Adjuntar los campos del formulario enviado.
        success: function(data) {
            if (data === "Ok") {
                alert("Actualización exitosa");
                setTimeout(function() {
                    window.location = "index.php?view=cliente"; //will redirect to your blog page (an ex: blog.html)
                }, 500);
            }
            else
            {
                alert("Error, verifica que todos los datos hayan sido llenados");
            }     
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}

// FUNCION QUE TOMA RECIBE UN ID COMO PARAMETRO Y ESTE MISMO LO ENVIA AL SERVICIO PARA SU ELIMINACION
function delCliente (id)
{
    var url = "https://omartrejo.000webhostapp.com/Services/Server/functions/delCliente.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: "id=" + id, // Adjuntar los campos del formulario enviado.
        success: function(data) {
           if (data === "Ok") {
                window.location = "index.php?view=cliente";
                
            } else {
               alert("Error, vuelve a intentarlo");
            }
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}

// FUNCTIN QUE OBTIENE TODOS LOS CLIENTES REGISTRADOS Y CON ESTATUS DE 1
function cargarClientes()
{
    var url = "https://omartrejo.000webhostapp.com/Services/Server/functions/clientes.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: 'id='+1,
        success: function(data) {
            //RECIBE YA COMO TAL UNA TABLA
            $("#bodyclientes").html(data);
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}
// FUNCION QUE OBTIENE LOS ESTADOS REGISTRADOS EN LA BASE DE DATOS
function cargarEstados ()
{
    var url = "https://omartrejo.000webhostapp.com/Services/Server/functions/estados.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        success: function(data) {
            var estados = JSON.parse(data);//RECIBE UN JSON
            $.each(estados, function(id,estado) { 
                //AGREGA LAS OPCIONES AL SELECT DE ESTADOS 
                $('#estado').append($('<option />', {
                    text: estado.nombre,
                    value: estado.idestado,
                }));
            }); 
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
    
}

// FUNCION QUE OBTIENE LOS ESTADOS QUE PERTENECEN AL ESTADO SELECCIONADO EN EL EVENTO ONCHANGE
function obtenerMunicipio()
{
    var id =  $('select[id=estado]').val();//TOMA EL VALOR SELECCIONADO EN EL COMBO DE ESTADOS
    var url = "https://omartrejo.000webhostapp.com/Services/Server/functions/municipios.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: "id=" + id, // Adjuntar los campos del formulario enviado.
        success: function(data) {
            $('#municipio').empty();//LIMPIA EL COMBO DE MUNICIPIOS
            var municipios = JSON.parse(data);//RECIBE UN JSON
            $.each(municipios, function(id,municipio) { 
                //AGREGA LAS OPCIONES AL COMBO DE MUNICIPIOS
                $('#municipio').append($('<option />', {
                    text: municipio.nombre,
                    value: municipio.idmunicipio,
                }));
            }); 
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}
//CARGA LOS DATOS DEL CLIENTE EN SUS RESPECTIVOS INPUTS
function cargarCliente()
{
    var id = $("#id").val();//TOMA EL VALOR QUE TIENE EL INPUT ID
    var url = "https://omartrejo.000webhostapp.com/Services/Server/functions/cargarCliente.php"; // El script a dónde se realizará la petición.
    $.ajax({
        type: "POST",
        url: url,
        data: "id=" + id, // Adjuntar los campos del formulario enviado.
        success: function(data) {
            
            var cliente = JSON.parse(data);//SE RECIBE UN JSON
            $("#nombre").val(cliente.nombre);
            $("#aPaterno").val(cliente.apellidoPaterno);
            $("#aMaterno").val(cliente.apellidoMaterno);
            $("#telefono").val(cliente.telefono);
            $("#clienteSAP").val(cliente.sap);
            $("#fechaCreacion").val(cliente.fechaCreacion);
            $("#nfiscal").val(cliente.nombre_fiscal);
            $("#rfc").val(cliente.rfc);
            $("#contacto").val(cliente.contacto);
            $("#estado").val(cliente.idestado);
            obtenerMunicipio();           //SE OBTIENEN LOS MUNICIPIOS DEL ESTADO SELECCIONADO 
            
            $("#latitud").val(cliente.latitud);
            $("#longitud").val(cliente.longitud);
            const params = cliente.latitud+","+cliente.longitud;
            reverseGeocoder(params);//SE CREA EL MARCADOR PARA EL MAPA

            $("#municipio").val(cliente.idmunicipio);
        }
    });

    return false; // Evitar ejecutar el submit del formulario.
}
