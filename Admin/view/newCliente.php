<!-- VISTA DEL FORMULARIO PARA EL REGISTRO DE UN NUEVO CLIENTE -->
<div class="card ">
    <div class="card-header ">
        <h4 class="card-title">Registro de cliente</h4>
        <p class="card-category">Este módulo es para registrar un nuevo cliente</p>
    </div>
    <div class="card-body ">
        <div class="align-content-center">
            <form action="" enctype="multipart/form-data" name="frmCliente" id="frmCliente" method="post">
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Nombre:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Apellido Paterno:</label>
                            <input type="text" class="form-control" name="aPaterno" id="aPaterno" placeholder="Apellido paterno" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Apellido Materno:</label>
                            <input type="text" class="form-control" name="aMaterno" id="aMaterno" placeholder="Apellido materno" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Telefono:</label>
                            <input type="text" class="form-control" maxlength="10" name="telefono" id="telefono" placeholder="Télefono" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Cliente SAP:</label>
                            <input type="text" class="form-control" name="clienteSAP" id="clienteSAP" placeholder="Cliente SAP" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Fecha de creación:</label>
                            <input type="date" class="form-control" name="fechaCreacion" id="fechaCreacion" value="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Nombre fiscal:</label>
                            <input type="text" class="form-control" name="nfiscal" id="nfiscal" placeholder="Nombre fiscal" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>RFC:</label>
                            <input type="text" class="form-control" maxlength="13" name="rfc" id="rfc" placeholder="RFC" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Contacto:</label>
                            <input type="text" class="form-control" name="contacto" id="contacto" placeholder="Contacto" value="">
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Estado:</label>
                            <select class="form-control" onchange="obtenerMunicipio()" name="estado" id="estado" placeholder="Estados">
                                <option value="0">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="form-group">
                            <label>Municipio:</label>
                            <select class="form-control" name="municipio" id="municipio" placeholder="Municipio">
                                <option value="0">Seleccionar</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">                    
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <label>Latitud:</label>
                            <input type="email" readonly class="form-control" name="latitud" id="latitud" placeholder="Latitud" value="">
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <label>Longitud:</label>
                            <input type="email" readonly class="form-control" name="longitud" id="longitud" placeholder="Longitud" value="">
                        </div>
                    </div>
                    <input type="hidden" name="prueba" id="prueba" value="0">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <div style="width: 100%; height: 480px" id="map"></div>
                        </div>
                    </div>
                </div>
            </form>
                <div class="row">                    
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label>Dirección:</label>
                            <input type="text" class="form-control" name="dire" id="dire" placeholder="Ejemplo de uso, '200 S Mathilda Ave, Sunnyvale, CA" value="">
                        </div>
                    </div>
                     <div class="col-md-3 ">
                        <div class="form-group">
                           <button class="btn btn-primary" onclick="geocoder()">Buscar</button>
                        </div>
                    </div>
                </div>
        </div>
        <hr>
        <div class="align-content-center">
            <button class="btn btn-success btn-lg" onclick="addCliente()"><i class="fa fa-save"></i> Registrar cliente</button>
        </div>
    </div>
</div>

<script src="js/here.js"></script>
<script>
    window.onload = function() {
        cargarEstados();//CARGA DE ESTADOS PARA EL SELECT, CONSUMIDO DEL SERVICIO
    };
</script>