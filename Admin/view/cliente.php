<!-- Este es solo la vista de la tabla principal con puro html -->

<div class="card">
    <div class="card-header">
        <h4 class="card-title"> Clientes registrados</h4>
    </div>
    <div class="card-body">
        <a href="index.php?view=newCliente" class="btn btn-success pull-right"> <i class="fa fa-plus"></i> Nuevo cliente</a> 
        <hr>
        <div class="table-responsive-sm">
            <table class="table clientes" id="reporte" name="clientes">
                <thead class=" text-primary">
                    <th>
                        Id
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Télefono
                    </th>
                    <th>
                        Cliente SAP
                    </th>
                    <th>
                        Fecha de creación
                    </th>
                    <th>
                        Nombre fiscal
                    </th>
                    <th>
                        RFC
                    </th>
                    <th>
                        Contacto
                    </th>
                    <th>
                        Estado/Municipio
                    </th>
                    <th>
                    
                    </th>
                </thead>
                <tbody id="bodyclientes">
                    
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    window.onload = function() {
        cargarClientes(); //ESTA FUNCION TRAE DE UN SERVICIO WEB LOS CLIENTES REGISTRADOS EN LA BASE DE DATOS
    };
</script>