<!-- VISTA DE ERROR CUANDO NO SE ENCUENTRA LA RUTA -->
<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h4 class="card-title">Página no encontrada</h4>
                <p class="card-category">No cuentas con permiso para este apartado.</p>
            </div>
            <div class="card-body ">
                <div class="align-content-center">
                    <img src="assets/img/notFound.jpg" class="img-fluid rounded mx-auto d-block" alt="Página no encontrada">
                </div>
                <hr>
                <div class="align-content-center">
                    <a href="index.php" class="btn btn-info btn-lg">Inicio</a>
                </div>
            </div>
        </div>
    </div>
</div>