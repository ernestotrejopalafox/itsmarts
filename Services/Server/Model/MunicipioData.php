<?php
//FUNCION COMO EL MODELO Y CONTROLADOR CON LAS FUNCIONES QUE HARA LA CLASE
    class MunicipioData
    {    
        public function __construct()
        {
            $this->idMunicipio = "";
            $this->nombre = "";       
        }       

        public static function getAll()
        {
            $query = "SELECT * FROM municipio";
            return Model::many($query,new MunicipioData());
        }

        public static function getAllByIdEstado($id)
        {
            $query = "SELECT m.* from municipio as m inner join estado_municipio as em on em.idmunicipio = m.idmunicipio inner join estado as e on e.idestado = em.idestado where e.idestado = '$id';";
            return Model::many($query,new MunicipioData());
        }

        public static function getById($id)
        {
            $query = "SELECT * FROM municipio WHERE idmunicipio = '$id'";
            return Model::one($query,new MunicipioData());
        }
    }
?>