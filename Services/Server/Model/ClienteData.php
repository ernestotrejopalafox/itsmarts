<?php

    //FUNCION COMO EL MODELO Y CONTROLADOR CON LAS FUNCIONES QUE HARA LA CLASE
    class ClienteData
    {    
        public function __construct()
        {
            $this->idcliente = "";
            $this->nombre = "";
            $this->apellidoPaterno = "";
            $this->apellidoMaterno = "";
            $this->telefono = "";
            $this->sap = 1;
            $this->fecha_registro = "";           
            $this->fechaCreacion = "";
            $this->nombre_fiscal = "";
            $this->rfc = "";
            $this->contacto = "";
            $this->idmunicipio = "";
            $this->idestado = "";
            $this->estatus = 1;
            $this->latitud = "";       
            $this->longitud = "";             
        }        

        public function add()
        {
            $query = insertsql("INSERT INTO cliente SET nombre = '$this->nombre', apellidoPaterno = '$this->apellidoPaterno', apellidoMaterno = '$this->apellidoMaterno', telefono = '$this->telefono', sap = '$this->sap' , fecha_registro = NOW(), fechaCreacion = '$this->fechaCreacion', nombre_fiscal = '$this->nombre_fiscal', rfc = '$this->rfc', contacto = '$this->contacto' , idmunicipio = '$this->idmunicipio', idestado = '$this->idestado', estatus = '$this->estatus' , latitud = '$this->latitud' , longitud = '$this->longitud'");
            return $query;
        }

        public static function getAll()
        {
            $query = "SELECT * FROM cliente WHERE estatus = '1'";
            return Model::many($query,new ClienteData());
        }

        public static function getById($id)
        {
            $query = "SELECT * FROM cliente WHERE idcliente = '$id'";
            return Model::one($query,new ClienteData());
        }

        
        public function del()
        {
            $query = sql("UPDATE cliente set estatus = '0' WHERE idcliente = '$this->idcliente'");
            return $query;
        }


        public function update()
        {
            $query = sql("UPDATE cliente SET nombre = '$this->nombre', apellidoPaterno = '$this->apellidoPaterno', apellidoMaterno = '$this->apellidoMaterno', telefono = '$this->telefono', sap = '$this->sap' , fecha_registro = NOW(), fechaCreacion = '$this->fechaCreacion', nombre_fiscal = '$this->nombre_fiscal', rfc = '$this->rfc', contacto = '$this->contacto' , idmunicipio = '$this->idmunicipio', idestado = '$this->idestado', estatus = '$this->estatus' , latitud = '$this->latitud' , longitud = '$this->longitud' WHERE idcliente = '$this->idcliente'");
            return $query;
        }


    }
?>