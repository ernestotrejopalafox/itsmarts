<?php
//FUNCION COMO EL MODELO Y CONTROLADOR CON LAS FUNCIONES QUE HARA LA CLASE
    class EstadoData
    {    
        public function __construct()
        {
            $this->idEstado = "";
            $this->nombre = "";       
        }       

        public static function getAll()
        {
            $query = "SELECT * FROM estado";
            return Model::many($query,new EstadoData());
        }

        public static function getById($id)
        {
            $query = "SELECT * FROM estado WHERE idestado = '$id'";
            return Model::one($query,new EstadoData());
        }
    }
?>