<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');


    if ($_SERVER['REQUEST_METHOD'] === 'POST') 
    {
        include("../autoload.php");

        $clientes = ClienteData::getAll();//RECIBE TODOS LOS CLIENTES REGISTADOS
        $response = NULL;
        if(count($clientes) > 0)
        {
            if($_POST['id'] == 1)
            {
                //RECORRE TODO EL ARREGLO DE OBJETO Y CREAR UNA TABLA DE DATOS
                foreach ($clientes as $cliente) 
                {
                    $response .= '
                    <tr>
                        <td>'.$cliente->idcliente.'</td>
                        <td>'.$cliente->nombre.' '.$cliente->apellidoPaterno.' '.$cliente->apellidoMaterno.'</td>
                        <td>'.$cliente->telefono.'</td>
                        <td>'.$cliente->sap.'</td>
                        <td>'.$cliente->fechaCreacion.'</td>
                        <td>'.$cliente->nombre_fiscal.'</td>
                        <td>'.$cliente->rfc.'</td>
                        <td>'.$cliente->contacto.'</td>
                        <td>'.MunicipioData::getById($cliente->idmunicipio)->nombre.', '.EstadoData::getById($cliente->idestado)->nombre.'</td>
                        <td>
                        <form action="index.php?view=editCliente" method="post">
                            <input type="hidden" name="id" id="id" value="'.$cliente->idcliente.'">
                            <button type="submit" class="btn btn-warning btn-sm">Editar</button>
                        </form>
                        <hr>
                        <button onclick="delCliente('.$cliente->idcliente.')" class="btn btn-danger btn-sm">Eliminar</button>
                        </td>
                    </tr>
                    ';
                }
                echo $response;
            }            
        }
    }
    else
    {
        echo "Método de conexión incorrecto";
    }
?>

