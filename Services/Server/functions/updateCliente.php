<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') 
    {
        include("../autoload.php");
        $cliente = ClienteData::getById($_POST['id']);//OBTIENE UN CLIENTE POR SU ID
        $cliente->nombre = $_POST['nombre'];
        $cliente->apellidoPaterno = $_POST['aPaterno'];
        $cliente->apellidoMaterno = $_POST['aMaterno'];
        $cliente->telefono = $_POST['telefono'];
        $cliente->sap = $_POST['clienteSAP'];         
        $cliente->fechaCreacion = $_POST['fechaCreacion'];
        $cliente->nombre_fiscal = $_POST['nfiscal'];
        $cliente->rfc = $_POST['rfc'];
        $cliente->contacto = $_POST['contacto'];
        $cliente->idmunicipio = $_POST['municipio'];
        $cliente->idestado = $_POST['estado'];
        $cliente->estatus = 1;
        $cliente->latitud = $_POST['latitud'];       
        $cliente->longitud = $_POST['longitud'];   
        $cliente->update();//ACTUALIZA DATOS
        if($cliente)
        {
            echo "Ok";
        }
        else
        {
            echo "Err";
        }
    }
    else
    {
        echo "Método de conexión incorrecto";
    }
?>