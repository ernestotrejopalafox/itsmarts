<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header("Access-Control-Allow-Origin: *");

    if ($_SERVER['REQUEST_METHOD'] === 'POST') 
    {
        include("../autoload.php");

        $estadoData = EstadoData::getAll();//OBTIENE TDOS LOS DATOS REGISTRADOS DE ESTA CLASE

        if(count($estadoData) > 0)
        {
            echo json_encode($estadoData);
        }
        else
        {
            echo "No encontro";
        }
    }
    else
    {
        echo "Método de conexión incorrecto";
    }
   
    

?>