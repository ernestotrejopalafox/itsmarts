<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') 
    {

        $cliente = ClienteData::getById($_POST['id']);//OBTIENE DATOS MEDIANTE EL ID
        if ($cliente) 
        {
            $cliente->del();//LO ELIMINA
            echo "Ok";
        }
        else
        {
            echo "No encontro";
        }
    }
    else
    {
        echo "Método de conexión incorrecto";
    }
   
    

?>